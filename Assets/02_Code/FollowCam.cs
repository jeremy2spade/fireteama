﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour {


	public Transform targetTr; //타켓의 트랜스폼 변수
	public float dist = 100.0f; //카메라와의 거리
	public float height = 3f; //카메라의 높이
	public float dampTrace = 500.0f; //부드러운 추적을위한 변수
	
	private Transform transform; //카메라 자신의 트랜스폼 변수


	// Use this for initialization
	void Start () {
		transform = GetComponent<Transform>();	//카메라 자신의 트랜스폼 컴포넌트를 transform 에 할당 

	}
	
	// Update is called once per frame
	void LateUpdate () {
		// 카메라의 위치 추적대상의 dist 변수만큼 뒤로 배치 
		// height 변수만큼 위로 올림

		transform.position = Vector3.Lerp(transform.position
									, targetTr.position - (targetTr.forward * dist) + (Vector3.up * height)
									, Time.deltaTime * dampTrace );

		//카메라가 타겟 게임오브젝트를 바라보게 설정
		transform.LookAt(targetTr.position);
		
	}
}
