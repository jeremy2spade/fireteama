﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorGizmo : MonoBehaviour {

	public Color _color = Color.magenta;
	public Color _sideColor = Color.yellow;
	public float _radius = 0.1f;
	// Use this for initialization

	void OnDrawGizmos(){
		Gizmos.color = _color;
		// Gizmos.DrawSphere(transform.position, _radius);
		Gizmos.DrawLine(new Vector3(-100,0,0), new Vector3(100,0,0));

		Gizmos.color = _sideColor;
		Gizmos.DrawLine(new Vector3(-100,0,-1), new Vector3(100,0,-1));
		Gizmos.DrawLine(new Vector3(-100,0,1), new Vector3(100,0,1));
	}
}
