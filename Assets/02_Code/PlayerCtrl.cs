﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
class PlayerStatus{
	private bool coverReady=false;
	public bool CoverReady{
		get{
			return coverReady;
		}set{
			coverReady = value;
		}
	}


	private bool movefreeze=false;
	public bool MoveFreeze{
		get{
			return movefreeze;
		}set{
			movefreeze = value;
		}
	}
	private int curStance = 2;
	public int CurStance{
		get{
			return curStance;
		}set{
			
			curStance += value;
			if(curStance<0){
				curStance = 0;
			}else if(curStance>2){
				curStance = 2;
			}
		}
	}

	




}
public class PlayerCtrl : MonoBehaviour {

	private int hp = 100;
	private float h = 0.0f;
	private float v = 0.0f;
	
	//이동 속도
	public float moveSpeed = 10.0f;
	//회전 속도 
	public float rotSpeed = 100.0f;
	
	public Transform firePos;
	private Transform transform;
	public Transform cube;

	Vector3 vectFirepos;
	Vector3 vectPlayer;
	Vector3 vectCube;
	
	//총구위치
	float fireLocationX = 0.86f;

	PlayerStatus playerSt = new PlayerStatus();



	public float[] hidePoint = new float[4];
	

	public List<GameObject> enemyList = new List<GameObject>();
	


	// Use this for initialization
	void Start () {
		//스크립트 처음에 Transform 컴포넌트를 할당
		transform = GetComponent<Transform>();


		 
		
	}
	

	
	
	// Update is called once per frame
	void Update () {
		
		
		// Debug.Log("Player cover ready: "+playerSt.CoverReady);
		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");
		//현재 위치 가져와 firepos 위치에 대입 
		vectFirepos = transform.position;
		vectFirepos.y = 2.54f;
		
		if(h>0){ //오른쪽vectorFirepos
			
			vectFirepos.x += fireLocationX;
			firePos.position = vectFirepos; //현재 위치에 변수를 넣어줌
			
		}else if(h<0){ //왼쪽
			
			vectFirepos.x += -fireLocationX;
			firePos.position = vectFirepos; //현재 위치에 변수를 넣어줌

		}else if(h==0){ //중립
			
		}
		
		// vectPlayer = transform.position;

		if(Input.GetKeyDown(KeyCode.Space) && playerSt.CoverReady == true && playerSt.MoveFreeze == false){ //은엄폐
			// Debug.Log("cover it!");
			//현재 테스트 전용 변경해야함
			//WallACtrl.cs 참고할것
			if(transform.position.x > hidePoint[3]){
				vectPlayer.x = (float)hidePoint[1];
				// Debug.Log("p1:"+hidePoint[3]);
			}else{
				vectPlayer.x = (float)hidePoint[0];
				// Debug.Log("p2:"+hidePoint[3]);
			}
			
			// vectPlayer.z = (float)hidePoint[2]; // Player 오브젝트 대신 Cube 를 이동시키기위해 변경
			vectCube.z = (float)hidePoint[2];

			transform.position = vectPlayer; //Player 오브젝트 위치 적용
			cube.position += vectCube; //Cube 오브젝트 위치 적용
			playerSt.MoveFreeze = true;
			

		}else if(Input.GetKeyDown(KeyCode.Space) && playerSt.MoveFreeze == true){
			// Debug.Log("cover out!");
			//현재 테스트 전용 변경해야함
			// vectPlayer.z = 0f; // Player 오브젝트 대신 Cube 를 이동시키기위해 변경
			vectCube.z = -(float)hidePoint[2];

			transform.position = vectPlayer; //Player 오브젝트 위치 적용
			cube.position += vectCube; //Cube 오브젝트 위치 적용
			playerSt.MoveFreeze = false;
		}
		
		
		// else if(Input.GetKey(KeyCode.S)){ //아래
		// 	vectPlayer.z = -1.0f;
		// 	transform.position = vectPlayer;
		// }
		// else{	//중립 
		// 	vectPlayer.z = 0f;
		// 	transform.position = vectPlayer;
		// }
		
		if(playerSt.MoveFreeze == false){
			//전후좌우 이동 방향 벡터 계산
			// Vector3 moveDir = (Vector3.forward * v) + (Vector3.right * h);
			Vector3 moveDir = (Vector3.right * h);
			// Translate(이동방향 * Time.deltaTime * 변위값 * 속도 , 기준좌표 )
			// transform.Translate(moveDir * moveSpeed * Time.deltaTime, Space.Self);aa
			transform.Translate(moveDir.normalized * moveSpeed * Time.deltaTime, Space.Self);
			
			// Vector3.up 축을 기준으로 rotSpeed 만큼 속도로 회전
			// transform.Rotate(Vector3.up * Time.deltaTime * rotSpeed * Input.GetAxis("Mouse X"));
			// transform.Rotate(Vector3.up * Time.deltaTime * rotSpeed * Input.GetAxis("left stick"));
		}else{}

		

		
		
	}//-----[end Update]



	//충돌 체크	
	void OnCollisionEnter(Collision coll){
		//투사체와 충돌체크
		if(coll.gameObject.tag == "BULLET"){
			Debug.Log("player hit!");
			//맞은 총알의 Damage를 추출해 플레이어 hp 차감
			hp -= coll.gameObject.GetComponent<BulletCtrl>().damage;
			if (hp <=0){	
				Debug.Log("player Down");
				Destroy(gameObject);
			}
			//Bullet 삭제
			Destroy(coll.gameObject);
		}
		
	}// -----[END OnCollisionEnter]
	
	void OnTriggerEnter(Collider coll){
		// Debug.Log("===="+coll.transform.position.x);
		
		//은폐물 트리거 체크 - Wall의 sphere collider 와 OnTrigger 가 발생할때 coverReady 키를 활성화
		if(coll.gameObject.tag == "UP_WALL_S"){
			// Debug.Log("hide ready!");
			playerSt.CoverReady = true;

			Vector3 position = coll.transform.position;
			// Debug.Log("wall position:"+position.x);
			
		}
		

		// Enemy List 에 트리거 걸린 Enemy 등록
		// if(coll.gameObject.tag == "ENEMY"){
		// 	// Debug.Log("Enemy : "+coll.gameObject);
		// 	enemyList.Add(coll.gameObject);
		
		// }
	
	}//-----[END OnTriggerEnter]

	void OnTriggerExit(Collider coll){
		//은폐물 트리거 체크 - Wall의 sphere collider 와 OnTrigger 가 발생할때 coverReady 키를 비 활성화
		if(coll.gameObject.tag == "UP_WALL_S"){
			// Debug.Log("hide not ready!");
			playerSt.CoverReady = false;
		}
		
	}//-----[END OnTriggerExit]


	//Wall 메세지 수신
	void ReceiveHidePoint(object[] msgObj){
		Debug.Log("!!Receive");
		hidePoint[0] = (float)msgObj[0];
		hidePoint[1] = (float)msgObj[1];
		hidePoint[2] = (float)msgObj[2];
		hidePoint[3] = (float)msgObj[3];
	}//-----[END ReceiveHidePoint]

}
