﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLeftCtrl : BulletCtrl {

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody>().AddForce(-transform.right * bSpeed);
		Destroy(gameObject,2f);
	}

}
