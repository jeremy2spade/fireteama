﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCtrl : MonoBehaviour {
	public GameObject bulletLeft;

	public Transform firePos;
	private float hp = 100f;
	private float damage;
	private float range;

	
	void Start(){

		
	}
	void Update(){
		if(Input.GetMouseButtonDown(2)){	
			Debug.Log("enemy : attack");
			Fire();
		}
	}
	void OnTriggerEnter(Collider coll){
		if(coll.gameObject.tag == "USER_CUBE"){
			InvokeRepeating("Fire",0.2f,1);
		}
	}// -----[END OnTriggerEnter]

		void OnTriggerExit(Collider coll){
		if(coll.gameObject.tag == "USER_CUBE"){
			CancelInvoke("Fire");
		}
	}// -----[END OnTriggerEnter]


	void OnCollisionEnter(Collision coll){
		//투사체와 충돌 체크
		if(coll.gameObject.tag == "BULLET"){
			Debug.Log("coll hit!");
			//맞은 총알의 Damage를 추출해 Enemy hp 차감
			hp -= coll.gameObject.GetComponent<BulletCtrl>().damage;
			if (hp <=0){
				Debug.Log("Enemy Down");
				Destroy(gameObject);
			}
			//Bullet 삭제
			Destroy(coll.gameObject);
		}
	}// -----[END OnCollisionEnter]




	void OnDamage(object[] _params){
		
		damage = (float)_params[1];
		range = (float)_params[2];

		Debug.Log("Ray hit"+damage/range);
		
		if(range<=30f){
			
			if(range>10f){
				hp -= damage * 0.2f;
			}else if(range>5f){
				hp -= damage * 0.4f;
			}else if(range>3f){
				hp -= damage * 0.7f;
			}else{
				hp -= damage;
			}
		}
		
		if (hp <=0){
				
			Debug.Log("Enemy Down - Ray");
			Destroy(gameObject);
		}
	}// -----[END OnDamage]


	void Fire(){
		//동적으로 총알생성
		CreateBullet();
	}// -----[END Fire]

	void CreateBullet(){

			//bulletLeft 프리팹을 동적으로 생성
			Instantiate(bulletLeft, firePos.position, firePos.rotation);
			Debug.DrawRay(firePos.position, -firePos.right * 1000.0f, Color.green);

	}// -----[END CreateBullet]


}
