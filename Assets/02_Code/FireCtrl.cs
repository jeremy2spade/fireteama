﻿using UnityEngine;

public class FireCtrl : MonoBehaviour {

	public GameObject bulletLeft;
	public GameObject bulletRight;

	public Transform firePos;

	//타이머용
	float timer;
	float waitingTime;
	//재장전 변수
	private bool reload =true;

	Vector3 direction;


	private float aimScale;
	private float aimPrec;
	void Start(){
		aimPrec = 8;
		// waitingTime = 0.0769f;
		waitingTime = 0.2f;
	}
	void Update(){
		timer += Time.deltaTime;
		Timer();

		if(Input.GetKey(KeyCode.LeftShift)){
			precision();
		}else if(Input.GetKey(KeyCode.LeftShift)==false){
			aimPrec = 8;
		}
		//마우스 좌클릭 발사
		if(Input.GetMouseButton(0)){	
			// Fire();
			Shoot1Weapon();
		// 마우스 우클릭 발사
		}else if(Input.GetMouseButtonDown(1)){
			Shoot2Weapon();
		}


	}
	//정조준을 위한 reduce
	void reduce(){
		if(aimPrec>0.5f){
			aimPrec -= 0.12f;
		}
	}
	//정조준
	void precision(){
		reduce();
		Debug.Log(aimPrec);

	}
	void Fire(){
		//동적으로 총알생성
		CreateBullet();
	}

	void Timer(){
		if(timer > waitingTime){
			timer = 0;
			reload = true;
		}
	}

	void Shoot1Weapon(){
		
		if(firePos.localPosition.x>0){
			direction = Vector3.right;
		}else{
			direction = Vector3.left;
		}
			

		//재장전 상태확인
		if(reload == true){
			Debug.Log("fire");
			RaycastHit hit;
			
			Debug.DrawRay(firePos.position, direction * 1000.0f, Color.green);	
			if(Physics.Raycast(firePos.position, direction, out hit, 1000.0f)){
				Debug.DrawRay(firePos.position, direction * 1000.0f, Color.red);	
				if(hit.collider.tag == "ENEMY"){
					object[] _params = new object[3];
					_params[0] = hit.point.x; //Ray 에 맞은 위치값
					_params[1] = 100f; //데미지
					_params[2] = Mathf.Abs((float)transform.position.x-(float)hit.point.x); //거리 (절대값적용)
					hit.collider.gameObject.SendMessage("OnDamage", _params, SendMessageOptions.DontRequireReceiver);
					// Debug.Log("hit point: "+_params[0]);
					// Debug.Log("fire point: "+transform.position.x);
					// Debug.Log("fight range: "+((float)transform.position.x-(float)_params[0]));
					Debug.Log("절대값: "+_params[2]);
					
					
				}
			}
	
			reload = false;
			timer = 0;
		}else{
			
		}

		
		
	}
	void Shoot2Weapon(){
		RaycastHit hit;
		Vector3 aimingPoint;
		
		if(firePos.localPosition.x>0){
			
			//aimPrec 값을 이용해 정조준을 반영한 aimScale 을 만든다
			aimScale = Random.Range(-1f*aimPrec, 1f*aimPrec);
			//aimScale 에 맞게 조준점 조정
			Quaternion rotation = Quaternion.identity; 
			rotation.eulerAngles = new Vector3(0f, 0f, aimScale); 
			firePos.rotation = rotation; 

			Debug.DrawRay(firePos.position, firePos.right * 1000.0f, Color.green);

			//Raycast 함수로 Ray 발사해 맞은 게임오브젝트가 존재하면 true
			if(Physics.Raycast(firePos.position, firePos.right, out hit, 1000.0f)){
				if(hit.collider.tag == "ENEMY"){
					object[] _params = new object[2];
					_params[0] = hit.point; //Ray 에 맞은 위치값
					_params[1] = 20; //데미지
					hit.collider.gameObject.SendMessage("OnDamage", _params, SendMessageOptions.DontRequireReceiver);

				}
			}
			
		}else{
			Debug.DrawRay(firePos.position, -firePos.right * 1000.0f, Color.green);
		}
		aimPrec = 8;
	}
	void CreateBullet(){
		if(firePos.localPosition.x>0){

			//bulletRight 프리팹을 동적으로 생성
			Instantiate(bulletRight, firePos.position, firePos.rotation);
			Debug.DrawRay(firePos.position, firePos.right * 1000.0f, Color.green);
		}else{

			//bulletLeft 프리팹을 동적으로 생성
			Instantiate(bulletLeft, firePos.position, firePos.rotation);
			Debug.DrawRay(firePos.position, -firePos.right * 1000.0f, Color.green);
		}
		
		
		
	}


}
