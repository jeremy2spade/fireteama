﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallACtrl : MonoBehaviour {
	public float xHide;
	public float zDepth;
	object[] _params = new object[4];
	

	// Use this for initialization
	void Start () {
		// float a =transform.position.x;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter(Collider coll){
		Debug.Log("Wall trigger ON STEP 1");
		
		if(coll.gameObject.tag =="USER_CUBE" && coll is BoxCollider){
			Debug.Log("Wall trigger ON STEP 2");
			_params[0] = transform.position.x-xHide; //왼쪽 은엄폐
			_params[1] = transform.position.x+xHide; //오른쪽 은엄폐
			_params[2] = transform.position.z-zDepth; //은엄폐 깊이
			_params[3] = transform.position.x;	//원위치

			//충돌발생시 Player 오브젝트의 ReceiveHidePoint 함수를 실행한다. transform.position를 전달한다. 
			coll.transform.parent.SendMessage("ReceiveHidePoint", _params, SendMessageOptions.DontRequireReceiver);

		}
	}
	


	
}
